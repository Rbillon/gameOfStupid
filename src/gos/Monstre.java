package gos;

import java.awt.Point;
import java.util.Random;

public class Monstre extends Personnage {

		private String forme;
		private int dureeVie;

		public Monstre() {
			this.forme = "M";
			this.dureeVie = randInt(1,6);
		}
		

		public String getForme() {
			return forme;
		}

		public void setForme(String forme) {
			this.forme = forme;
		}

		public int getDureeVie() {
			return dureeVie;
		}

		public void setDureeVie(int dureeVie) {
			this.dureeVie = dureeVie;
		}

		
		public int randInt(int min, int max) {
	        Random rand = new Random();
	        int randomNum = rand.nextInt((max - min) + 1) + min;

	        return randomNum;
	    }
		
		
		
}
