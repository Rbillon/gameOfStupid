package gos;

public class Hero extends Personnage {
	
	private String forme;

	public Hero() {
		this.forme = "H";
	}

	public String getForme() {
		return forme;
	}

	public void setForme(String forme) {
		this.forme = forme;
	}

}
