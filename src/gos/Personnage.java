package gos;

public class Personnage {

		private String forme;
		
		public String getForme() {
			return forme;
		}

		public void setForme(String forme) {
			this.forme = forme;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((forme == null) ? 0 : forme.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Personnage other = (Personnage) obj;
			if (forme == null) {
				if (other.forme != null)
					return false;
			} else if (!forme.equals(other.forme))
				return false;
			return true;
		}
		
		
}
