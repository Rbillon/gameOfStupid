package gos;

import java.awt.Point;
import java.lang.reflect.Field;
import java.util.*;
import java.util.Map.Entry;

public class Plateau {
	
	private int x;
	private int y;
	private int taillePlateau;
	private int nbMonstres;
	private Map<Point, Cellule> listeCellules = new LinkedHashMap<>();
	private Map<Point, Monstre> listeMonstres = new LinkedHashMap<>();
	
	private Point premiereCase;
	private Point derniereCase;
	private Point actuelHero;
	private Point nouveauHero;
	
	private int nbTours;
	private boolean finJeu = false;
	private boolean surPrincesse;
	private boolean surMonstre;
	
	public Plateau(int x, int y) {
		this.x = x;
		this.y = y;
		this.taillePlateau = x * y;
		this.nbMonstres = taillePlateau * 18 / 100;
		this.premiereCase = new Point(0,0);
		this.derniereCase = new Point(x-1, y-1);
		this.actuelHero = actuelHero;
		
        
        // Liste de monstres avec coordonnees aleatoires
        for (int m = 0; m < nbMonstres; m++) {
            Monstre monstre = new Monstre();
            Cellule cellule = new Cellule(monstre);
            pointMonstre(monstre, x, y);
        }


		// Generation du plateau
		for(int i = 0; i < y; i++) {
			for(int j = 0; j < x; j++) {
				
				Point coordonnee = new Point(j,i);
				
				if(coordonnee.equals(premiereCase)) {
					Hero hero = new Hero();
					Cellule cellule = new Cellule(hero);
					listeCellules.put(coordonnee, cellule);
				}
				else if(coordonnee.equals(derniereCase)){
					Princesse princesse = new Princesse();
					Cellule cellule = new Cellule(princesse);
					listeCellules.put(coordonnee, cellule);
				}
				else if(listeMonstres.containsKey(coordonnee)) {
					Monstre monstre = new Monstre();
					Cellule cellule = new Cellule(monstre);
					listeCellules.put(coordonnee, cellule);
				}
				else {
					Cellule cellule = new Cellule("X");
					listeCellules.put(coordonnee, cellule);
				}
			}
		}
		
		Set<Entry<Point, Monstre>> setListeMonstres = listeMonstres.entrySet();
	      Iterator<Entry<Point, Monstre>> it = setListeMonstres.iterator();
	      while(it.hasNext()){
	    	  Entry<Point, Monstre> e = it.next();
	         System.out.println(e.getKey() + " : " + e.getValue());
	      }
	    
		
		while(finJeu == false) {
			this.nbTours++;
			//this.ancienHero = this.premiereCase;
			
			if(nbTours == 1) {
				this.actuelHero = this.premiereCase;
				//System.out.println(actuelHero);
			}
			
			if(actuelHero.equals(derniereCase)) {
				finJeu = true;
				surPrincesse = true;
			}
			else if(listeMonstres.containsKey(actuelHero)) {
				finJeu = true;
				surMonstre = true;
			}
			else {
				finJeu = false;
			}
			
			
			printPlateau(x, y, listeCellules, actuelHero);
			modifierPlateau(actuelHero);
			
			
			if(finJeu == true) {
				if(surPrincesse == true) {
					System.out.println("Vous avez sauve la princesse en " + nbTours + " tours");
				}
				else if(surMonstre == true) {
					System.out.println("Un monstre vous a tue. Vous avez resiste " + nbTours + " tours");
				}
		    }
			System.out.println("");
		}
	}

	public Map<Point, Cellule> getListeCellules() {
		return listeCellules;
	}

	public void setListeCellules(Map<Point, Cellule> listeCellules) {
		this.listeCellules = listeCellules;
	}

	public Map<Point, Monstre> getListeMonstres() {
		return listeMonstres;
	}

	public void setListeMonstres(Map<Point, Monstre> listeMonstres) {
		this.listeMonstres = listeMonstres;
	}

	public int randInt(int min, int max) {
		if(min > max) {int temp; temp = max; max = min; min = temp;}
        Random rand = new Random();
        int randomNum = rand.nextInt((max - min) + 1) + min;

        return randomNum;
    }
	
	public void pointMonstre(Monstre monstre, int x, int y) {
		this.derniereCase = new Point(x-1, y-1);
		Point coordonneeMonstre = new Point(randInt(1,x-1),randInt(1, y-1));

		Map<Point, Monstre> listeMonstre = getListeMonstres();
		
		if(! listeMonstre.containsKey(coordonneeMonstre) && !coordonneeMonstre.equals(derniereCase)) {
			listeMonstre.put(coordonneeMonstre, monstre);
			setListeMonstres(listeMonstre);
		}
		else {
			pointMonstre(monstre, x, y);
		}
	}
	
	public void printPlateau(int x, int y, Map<Point, Cellule> listeCellules, Point actuelHero){
		this.derniereCase = new Point(x-1, y-1);
		
		for (int i = 0; i < y; i++) {
		        for (int j = 0; j < x; j++) {
		            Point coordonnee = new Point(j, i);
		            
		            if(coordonnee.equals(actuelHero)) {
						Hero hero = new Hero();
						System.out.print("| " + hero.getForme() + " |");
					}
					else if(coordonnee.equals(derniereCase)){
						Princesse princesse = new Princesse();
						System.out.print("| " + princesse.getForme() + " |");
					}
					else if(listeMonstres.containsKey(coordonnee)) {
						Monstre monstre = new Monstre();
						System.out.print("| " + monstre.getForme() + " |");
						//System.out.print(monstre.getDureeVie());
					}
					else {
						Cellule cellule = new Cellule("X");
						System.out.print("| . |");
					}
		        }
		        System.out.println("");
		    }
	}
	
	// On redessine le plateau
	public void modifierPlateau(Point actuelHero) {
		vieMonstre(listeMonstres, nbTours);
		deplaceHero(actuelHero);
		
	}
	
	public void deplaceHero(Point actuelHero) {
		boolean valide = false;
		System.out.print(this.actuelHero);
		
		Cellule cellule = new Cellule("X");
		listeCellules.put(actuelHero, cellule);
		
		Point nouveauHero = actuelHero;
		nouveauHero.move(randInt((int) nouveauHero.getX() - 1, (int) nouveauHero.getX() + 1), randInt((int) nouveauHero.getY() - 1, (int) nouveauHero.getY() + 1));

		// On déplace le héros uniquement dans une case appartenant au plateau
		while(valide == false) {
			if(listeCellules.containsKey(nouveauHero)){
				valide = true;
			}
			else{
				valide = false;
				nouveauHero = actuelHero;
				nouveauHero.move(randInt((int) nouveauHero.getX() - 1, (int) nouveauHero.getX() + 1), randInt((int) nouveauHero.getY() - 1, (int) nouveauHero.getY() + 1));
			}
		}
		if(valide == true) {
		Hero hero = new Hero();
		Cellule cellule2 = new Cellule(hero);
		listeCellules.put(nouveauHero, cellule2);
		}

	}

	public void vieMonstre (Map<Point, Monstre> listeMonstres, int tour) {
		Map<Point, Monstre> monstreAAjouter = new HashMap<>();
		Map<Point, Monstre> monstreASupprimer = new HashMap<>();
		listeMonstres.forEach((point, monstre) -> {
			if(tour > monstre.getDureeVie()) {
				Map<Point, Cellule> nouvelleListeCellule = getListeCellules();

				// On remplace l'ancienne cellule Monstre par une cellule vide
				Cellule cellule = new Cellule("X");
				nouvelleListeCellule.put(point, cellule);
				setListeCellules(nouvelleListeCellule);

				// On supprime le monstre de la liste des monstres
				monstreASupprimer.put(point, monstre);


				// On créé un nouveau monstre
				Monstre nouveauMonstre = new Monstre();
				monstreAAjouter.put(point, nouveauMonstre);

			}
		});

		// On le fait après le foreach pour eviter les null pointer
		monstreAAjouter.forEach((point, monstre) -> {
			pointMonstre(monstre, this.x, this.y);
		});

		monstreASupprimer.forEach((point, monstre) -> {
			Map<Point, Monstre> listeAJour = getListeMonstres();
			listeAJour.remove(point);
			setListeMonstres(listeAJour);
		});
	}
	
	
	
	

}
